package com.jtest;

import java.util.Date;

import biz.lowcostflights.LowcostFlightSupplierWS;
import biz.lowcostflights.LowcostFlightSupplierWS.CheapFlight;
import biz.regularflights.RegularFlightSupplierWS;
import biz.regularflights.RegularFlightSupplierWS.Connection;

public class Main {

    public static void main(String []args) throws Exception {
        //We've setup Maven & Gradle for your convenience. Please feel free to pick one (you can remove other config files for clarity)
        //
        //The CLI should work using one of the following commands:
        //
        // mvn clean install exec:java
        // gradle clean run
        //

        //Sample data from the fake services. Please feel free to remove this code entirely and/or replace the demo
        //with an integration test.
        String destCode = "AMS";
        String deptCode = "BCN";

        Date date = new Date();
        Integer maxResults = 5;

        LowcostFlightSupplierWS lowcostSupplier = new LowcostFlightSupplierWS();

        System.out.println("Cheap flights: ");
        for (CheapFlight flight : lowcostSupplier.list(destCode, deptCode, date, maxResults)) {
            System.out.println(flight.toString());
        }

        RegularFlightSupplierWS regularSupplier = new RegularFlightSupplierWS();

        System.out.println("Regular flights: ");
        for (Connection connection : regularSupplier.getConnections(destCode, deptCode, date, maxResults)) {
            System.out.println(connection.toString());
        }
    }

}


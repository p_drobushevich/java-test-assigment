package com.jtest.aggregator

import akka.actor.Actor
import biz.regularflights.RegularFlightSupplierWS
import biz.regularflights.RegularFlightSupplierWS.Connection

import scala.collection.JavaConversions._

class RegularFlight(service: RegularFlightSupplierWS) extends Actor {

  override def receive = {
    case query: FlightQuery =>
      try {
        val result = service
          .getConnections(query.destCode, query.deptCode, query.date, query.maxResults)
          .toList.map(convert)
        sender ! result
      } catch {
        case e: java.net.ConnectException =>
          sender ! akka.actor.Status.Failure(e)
      }
  }

  private def convert(c: Connection): Flight =
    Flight(c.getId, c.getDestinationCode, c.getDepartureCode, c.getDateTime.toDate,
      c.getPrice.getAmount.toString, c.getPrice.getCurrency.toString)

}
package com.jtest.aggregator

import akka.actor.{Props, _}
import akka.pattern.ask
import akka.util.Timeout
import biz.lowcostflights.LowcostFlightSupplierWS
import biz.regularflights.RegularFlightSupplierWS

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

trait FlightService {

  def asyncSearch(flightQuery: FlightQuery): Future[List[Flight]]

  def search(flightQuery: FlightQuery): List[Flight]

}

class FlightServiceImpl extends FlightService {

  implicit val timeout = Timeout(40 seconds)

  val aggregator = {
    val ctx = TypedActor.context
    val lowCost = ctx.actorOf(Props(new LowCostFlight(new LowcostFlightSupplierWS())))
    val regular = ctx.actorOf(Props(new RegularFlight(new RegularFlightSupplierWS())))
    ctx.actorOf(Props(new Aggregator(lowCost :: regular :: List())))
  }

  def asyncSearch(flightQuery: FlightQuery): Future[List[Flight]] = {
    (aggregator ? flightQuery).mapTo[List[Flight]]
  }

  def search(flightQuery: FlightQuery): List[Flight] = {
    Await.result(asyncSearch(flightQuery), 40 second)
  }
}
package com.jtest.aggregator

import akka.actor.{Actor, ActorRef}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import akka.util.Timeout._

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

class Aggregator(var fetchers: List[ActorRef]) extends Actor {

  implicit val timeout: Timeout = 30.second

  override def receive = {
    case query: FlightQuery =>
      val fetcherFeatures = fetchers.map(f = fetcher => {
        (fetcher ? query).recover {
          case ex: Throwable => List.empty
        }
      })
      Future.sequence(fetcherFeatures)
        .mapTo[List[List[Flight]]]
        .map(l => l.flatten) pipeTo sender
  }

}

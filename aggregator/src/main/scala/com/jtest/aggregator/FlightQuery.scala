package com.jtest.aggregator

import java.util.Date

case class FlightQuery(destCode: String, deptCode: String, date: Date, maxResults: Integer)
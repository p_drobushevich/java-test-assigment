package com.jtest.aggregator

import java.util.Date

case class Flight(id: String, destCode: String, deptCode: String, date: Date, price: String, currency: String)
package com.jtest.aggregator

import java.util.Date

import akka.actor.{ActorSystem, _}
import akka.util.Timeout

import scala.concurrent.duration._
import scala.language.postfixOps

object Main {

  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem("Flight")

    implicit val timeout = Timeout(40 seconds)

    val service: FlightService = TypedActor(system)
      .typedActorOf(TypedProps[FlightServiceImpl]
      .withTimeout(timeout))
    val query = FlightQuery("1", "2", new Date(), 2)
    val result = service.search(query)

    result.foreach(println)

    system.shutdown
  }

}

class Main
package com.jtest.aggregator

import java.net.ConnectException

import akka.actor.Actor
import biz.lowcostflights.LowcostFlightSupplierWS
import biz.lowcostflights.LowcostFlightSupplierWS.CheapFlight

import scala.collection.JavaConversions._

class LowCostFlight(service: LowcostFlightSupplierWS) extends Actor {

  override def receive = {
    case query: FlightQuery =>
      try {
        val result = service
          .list(query.destCode, query.deptCode, query.date, query.maxResults)
          .toList.map(convert)
        sender ! result
      } catch {
        case e: ConnectException =>
          sender ! akka.actor.Status.Failure(e)
      }
  }

  private def convert(ch: CheapFlight): Flight =
    Flight(ch.getId.toString, ch.getDestCode, ch.getDeptCode, ch.getTime, ch.getPrice, ch.getCurrency)

}
package com.jtest.aggregator

import java.math.BigDecimal
import java.net.ConnectException
import java.util.{Currency, Date}

import akka.actor.Status.Failure
import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit}
import biz.regularflights.RegularFlightSupplierWS
import biz.regularflights.RegularFlightSupplierWS.Money
import org.joda.time.{Duration, LocalDateTime}
import org.mockito.Mockito._

import scala.collection.JavaConversions._

class RegularFlightSpec(_system: ActorSystem) extends BaseActorSpec(_system) {

  def this() = this(ActorSystem("RegularFlightSpec"))

  override def afterAll() = {
    TestKit.shutdownActorSystem(system)
  }

  "An RegularFlight actor" must {

    val destCode = "LON"
    val deptCode = "AMS"
    val date = new Date()
    val maxResult = 5

    "should send query result" in {
      val query = FlightQuery(destCode, deptCode, date, maxResult)

      val service = mock[RegularFlightSupplierWS]

      when(service.getConnections(destCode, deptCode, date, maxResult))
        .thenReturn(List(new RegularFlightSupplierWS.Connection("1", destCode, deptCode, new LocalDateTime(date),
        new Money(new BigDecimal(1), Currency.getInstance("USD")), Duration.standardHours(1))))

      val actor = TestActorRef(Props(new RegularFlight(service)))
      actor ! query
      expectMsg(List(Flight("1", destCode, deptCode, date, "1", "USD")))
    }

    "handle service error" in {
      val query = FlightQuery(destCode, deptCode, date, maxResult)

      val service = mock[RegularFlightSupplierWS]

      val exception = new ConnectException("Connection failed!")
      when(service.getConnections(destCode, deptCode, date, maxResult)).thenThrow(exception)

      val actor = TestActorRef(Props(new RegularFlight(service)))
      actor ! query
      expectMsgPF() {
        case Failure(exception) ⇒ ()
      }
    }

  }
}
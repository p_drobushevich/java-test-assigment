package com.jtest.aggregator

import java.util.Date

import akka.actor.Status.Failure
import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit}

class FakeFlight(query: FlightQuery, result: List[Flight]) extends Actor {
  override def receive = {
    case query => sender ! result
  }
}

class FailFlight(query: FlightQuery) extends Actor {
  override def receive = {
    case query => sender ! Failure(new Exception())
  }
}

class AggregatorSpec(_system: ActorSystem) extends BaseActorSpec(_system) {

  def this() = this(ActorSystem("AggregatorSpec"))

  override def afterAll() = {
    TestKit.shutdownActorSystem(system)
  }

  "An Aggregator actor" must {

    val destCode = "LON"
    val deptCode = "AMS"
    val date = new Date()
    val maxResult = 5

    val query = FlightQuery(destCode, deptCode, date, maxResult)

    "merge result" in {
      val flight1 = Flight("1", destCode, deptCode, date, "1", "USD")
      val fakeFlight1 = TestActorRef(Props(new FakeFlight(query, List(flight1))))
      val flight2 = Flight("2", destCode, deptCode, date, "2", "EUR")
      val fakeFlight2 = TestActorRef(Props(new FakeFlight(query, List(flight2))))
      val actor = TestActorRef(Props(new Aggregator(List(fakeFlight1, fakeFlight2))))

      actor ! query
      expectMsg(List(flight1, flight2))
    }

    "ignore error" in {
      val flight1 = Flight("1", destCode, deptCode, date, "1", "USD")
      val fakeFlight1 = TestActorRef(Props(new FakeFlight(query, List(flight1))))
      val fakeFlight2 = TestActorRef(Props(new FailFlight(query)))
      val actor = TestActorRef(Props(new Aggregator(List(fakeFlight1, fakeFlight2))))

      actor ! query
      expectMsg(List(flight1))
    }

  }
}
package com.jtest.aggregator

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

abstract class BaseActorSpec(_system: ActorSystem) extends TestKit(_system)
with ImplicitSender
with Matchers
with WordSpecLike
with MockitoSugar
with BeforeAndAfterAll
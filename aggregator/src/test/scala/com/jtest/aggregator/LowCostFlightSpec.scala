package com.jtest.aggregator

import java.net.ConnectException
import java.util.Date

import akka.actor.Status.Failure
import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit}
import biz.lowcostflights.LowcostFlightSupplierWS
import org.mockito.Mockito._

import scala.collection.JavaConversions._

class LowCostFlightSpec(_system: ActorSystem) extends BaseActorSpec(_system) {

  def this() = this(ActorSystem("LowCostFlightSpec"))

  override def afterAll() = {
    TestKit.shutdownActorSystem(system)
  }

  "An LowCostFlight actor" must {

    val destCode = "LON"
    val deptCode = "AMS"
    val date = new Date()
    val maxResult = 5

    "send query result" in {
      val query = FlightQuery(destCode, deptCode, date, maxResult)

      val service = mock[LowcostFlightSupplierWS]

      when(service.list(destCode, deptCode, date, maxResult))
        .thenReturn(List(new LowcostFlightSupplierWS.CheapFlight(1l, destCode, deptCode, date, 1, "1", "USD")))

      val actor = TestActorRef(Props(new LowCostFlight(service)))
      actor ! query
      expectMsg(List(Flight("1", destCode, deptCode, date, "1", "USD")))
    }

    "handle service error" in {
      val query = FlightQuery(destCode, deptCode, date, maxResult)

      val service = mock[LowcostFlightSupplierWS]

      val exception = new ConnectException("Connection failed!")
      when(service.list(destCode, deptCode, date, maxResult)).thenThrow(exception)

      val actor = TestActorRef(Props(new LowCostFlight(service)))
      actor ! query
      expectMsgPF() {
        case Failure(exception) ⇒ ()
      }
    }

  }
}
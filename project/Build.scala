import play.PlayScala
import sbt.Keys._
import sbt._

object Dependencies {

  var akkaVersion = "2.3.9"

  val jodaTime = "joda-time" % "joda-time" % "2.5"
  val junit = "junit" % "junit" % "4.11" % Test
  val mockito = "org.mockito" % "mockito-all" % "1.9.5" % Test

  val akka = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  val akkaTestKit = "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test
  val scalaTest = "org.scalatest" % "scalatest_2.11" % "2.2.3" % Test

}

object BuildSettings {
  val buildVersion = "1.0.0"
  val buildScalaVersion = "2.11.5"

  val buildSettings = Seq(
    version := buildVersion,
    scalaVersion := buildScalaVersion
  )
}

object TravelBuild extends Build {

  import BuildSettings._
  import Dependencies._

  val servicesDeps = Seq(
    jodaTime,
    junit,
    mockito
  )

  val aggregatorDeps = Seq(
    akka,
    mockito,
    akkaTestKit,
    scalaTest
  )

  val webDeps = Seq(
    akka
  )

  lazy val root = Project(
    id = "travel",
    base = file(".")
  ) aggregate(services, aggregator, web)

  lazy val services = Project(
    id = "services",
    base = file("services"),
    settings = buildSettings ++ Seq(libraryDependencies ++= servicesDeps)
  )

  lazy val aggregator = Project(
    id = "aggregator",
    base = file("aggregator"),
    settings = buildSettings ++ Seq(libraryDependencies ++= aggregatorDeps)
  ) dependsOn services

  lazy val web = Project(
    id = "web",
    base = file("web"),
    settings = buildSettings ++ Seq(libraryDependencies ++= webDeps)
  ).settings(
      mainClass in(Compile, run) := Some("play.core.server.NettyServer")
    ).enablePlugins(PlayScala) dependsOn aggregator
}

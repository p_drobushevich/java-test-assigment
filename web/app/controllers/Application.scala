package controllers

import java.util.Date

import akka.actor.{TypedActor, TypedProps}
import akka.util.Timeout
import com.jtest.aggregator.{Flight, FlightQuery, FlightService, FlightServiceImpl}
import play.api.Play.current
import play.api.libs.concurrent._
import play.api.mvc.{Action, Controller}

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object Application extends Controller {

  implicit val timeout = Timeout(40 seconds)

  val service: FlightService = TypedActor(Akka.system)
    .typedActorOf(TypedProps[FlightServiceImpl]
    .withTimeout(timeout))

  val query = FlightQuery("LON", "AMS", new Date(), 5)

  def index() = Action.async {
    service.asyncSearch(query).map(flights ⇒ Ok(views.html.index(flights)))
  }

}